# test-pod

[![CI Status](http://img.shields.io/travis/Julian Builes/test-pod.svg?style=flat)](https://travis-ci.org/Julian Builes/test-pod)
[![Version](https://img.shields.io/cocoapods/v/test-pod.svg?style=flat)](http://cocoadocs.org/docsets/test-pod)
[![License](https://img.shields.io/cocoapods/l/test-pod.svg?style=flat)](http://cocoadocs.org/docsets/test-pod)
[![Platform](https://img.shields.io/cocoapods/p/test-pod.svg?style=flat)](http://cocoadocs.org/docsets/test-pod)

## Usage

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

test-pod is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

    pod "test-pod"

## Author

Julian Builes, jlnbuiles@gmail.com

## License

test-pod is available under the MIT license. See the LICENSE file for more info.

